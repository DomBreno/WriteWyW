// <Loading Modules>
	require('dotenv').config();
	require('module-alias/register');
	const mongoose = require('mongoose');
	const express = require('express');
//</Loading Modules>

// <Other Modules>
	const boot = require('@service/boot');
	const config = require("@config");
	const app = express();
// </Other Modules>

// <Mongoose Config>
	mongoose.set('useNewUrlParser', true);
	mongoose.set('useFindAndModify', false);
	mongoose.set('useCreateIndex', true);
// </Mongoose Config>

// <Start Application>
	if (config.db.connectionString) {
		mongoose.connect(config.db.connectionString, boot)
	}
// </Start Application>