const normalizePort = require('normalize-port');

const port = normalizePort(process.env.PORT || 8080);

module.exports = {
    app: {
        port,
    },
    db: {
        connectionString: process.env.MONGO_DB,
    },
};