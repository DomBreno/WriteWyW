const app = require('@root/app');
const config = require('@config');

app.use((req, res, next) => {
  const error = new Error('Not Found');
  error.status = 404;
  error.numbers = Object.keys(error);

  res.render('error', {
    status: error.status,
    length: error.numbers.length,
    title: 'Page Not Found - Write WYW',
    imageTitle: '/static/img/404-error.png'
  });
});

module.exports = (err) => {
  if (err) throw console.error('Erro ao Conectar no Banco de Dados', err);
  console.log('Database Connection Success: "BloggerDevB"');

  app.listen(config.app.port, (err) => {
    if (err) throw console.error('Erro ao Ligar-se ao Servidor', err);
    console.log(`Servidor Online: http://localhost:${config.app.port}`);
  });
};