const router = require('express').Router();

router.get('/', (req, res, next) => {
    res.render('admin/index', {
        title: 'Admin Page - Write WYW',
        imageTitle: '/static/img/admin.png',
    });
});

router.get('/posts', (req, res, next) => {
    res.send('Posts Page!');
});

router.get('/categorys', (req, res, next) => {
    res.render('admin/categorys', {
        title: 'Categorys Page - Write WyW'
    });
});

router.get('/categorys/add', (req, res, next) => {
    res.render('admin/addCategorys', {
        title: 'Add Categorys Page - Write WyW'
    });
});

module.exports = router;