// <Loading Modules>
	const express = require('express');
	const bodyParser = require('body-parser');
	const hbs = require('express-handlebars');
	const cors = require('cors');
// </Loading Modules>

// <Loading Routes>
	const admin = require("@routes/admin")
// </Loading Routes>

const app = express();// </Initialize Express>

// <Common Configs: Body-parser; Handlebars; Statics Files;>
	app.use(bodyParser.urlencoded({ extended: true }));
	app.use(bodyParser.json())

	app.engine('.hbs', hbs({ extname: '.hbs', defaultLayout: 'main' }));
	app.set('view engine', '.hbs');

	app.use('/static', express.static('public'));
	
	app.use(cors());
// </Common Configs: Body-parser && Handlebars; Statics Files;>

// <Routes>
	app.use('/admin', admin);
// </Routes>

module.exports = app;